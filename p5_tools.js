let KEY_REPEAT_TIME = 0.1; //seconds
let KEY_HOLD_TIME = 0.2; // seconds

let keyboardHandlers = [];

class KeyboardEventHandler {
    constructor() {
        this.itemsRegisteredForKeyPressed = [];
        this.itemsRegisteredForKeyTapped = [];
        this.itemsRegisteredForKeyHeld = [];
        this.itemsRegisteredForKeyRepeated = [];
        this.itemsRegisteredForKeyReleased = [];

        this.keyDownTimeStamp = null;
        this.heldEventAlreadyTriggered = false;

        keyboardHandlers.push(this);
    }

    ListenFor(item, eventName, typeOfCharacter) {
        switch (eventName) {
            case "KeyPressed":
                this.ListemForKeyPressed(item, typeOfCharacter);
                break;
            case "KeyTapped":
                this.ListemForKeyTapped(item);
                break;
            case "KeyHeld":
                this.ListemForKeyHeld(item);
                break;
            case "KeyRepeated":
                this.ListemForKeyRepeated(item);
                break;
            case "KeyReleased":
                this.ListemForKeyReleased(item);
                break;
        }
    }

    ListemForKeyPressed(item, typeOfCharacter) {
        this.itemsRegisteredForKeyPressed.push([item, typeOfCharacter]);
    }

    ListemForKeyTapped(item) {
        this.itemsRegisteredForKeyTapped.push(item);
    }

    ListemForKeyHeld(item) {
        this.itemsRegisteredForKeyHeld.push(item);
    }

    ListemForKeyRepeated(item) {
        this.itemsRegisteredForKeyRepeated.push(item);
    }

    ListemForKeyReleased(item) {
        this.itemsRegisteredForKeyReleased.push(item);
    }

    _KeyWentDown() {
        console.log("_KeyWentDown", key);
        this.keyDownTimeStamp = time();
        this.itemsRegisteredForKeyPressed.forEach(tup => {
            let item = tup[0];
            let typeOfCharacter = tup[1];

            if (typeOfCharacter == "PrintableOnly") {
                if (IsPrintable(keyCode)) {
                    item.KeyPressed(key);
                    return;
                }
            } else {
                item.KeyPressed(key);
            }
        });
    }

    _KeyWentUp() {
        console.log("_KeyWentUp", key);
        this.keyDownTimeStamp = null;
        this.heldEventAlreadyTriggered = false;

        this.itemsRegisteredForKeyReleased.forEach(item => {
            item.KeyReleased(key);
        });
    }

    _KeyWentRepeated() {
        console.log("_KeyWentRepeated", key);
        this.itemsRegisteredForKeyRepeated.forEach(item => {
            item.KeyRepeated(key);
        });
    }

    _KeyWentHeld() {
        console.log("_KeyWentHeld", key);
        this.itemsRegisteredForKeyHeld.forEach(item => {
            item.KeyHeld(key);
        });
        this.REPEAT_RATE_IN_FRAMES = Math.round(KEY_REPEAT_TIME * frameRate());
    }

    Update() {
        //check for held down keys
        if (keyIsPressed && this.keyDownTimeStamp) {
            // a key is being pressed
            let heldFor = time() - this.keyDownTimeStamp;
            //   console.log("heldFor=", heldFor, ", keyDownTimeStamp=", this.keyDownTimeStamp);
            if (heldFor > KEY_HOLD_TIME) {
                if (this.heldEventAlreadyTriggered == false) {
                    // held event
                    this.heldEventAlreadyTriggered = true;
                    console.log("held event");
                    this._KeyWentHeld();
                }

                console.log("frameRate()=", frameRate());
                console.log("REPEAT_RATE_IN_FRAMES=", this.REPEAT_RATE_IN_FRAMES);
                if (frameCount % this.REPEAT_RATE_IN_FRAMES == 0) {
                    // repeat event
                    console.log("repeat event");
                    this._KeyWentRepeated();
                }
            }
        }
    }
}

let mouseHandlers = [];

class MouseEventHanlder {
    constructor() {
        this.lastMouseDownVector = null;

        this.itemsRegisteredForMousePressed = [];
        this.itemsRegisteredForMouseReleased = [];
        this.itemsRegisteredForMouseDragged = [];
        this.itemsRegisteredForMouseWheel = [];
        this.itemsRegisteredForMouseDoubleClicked = [];

        mouseHandlers.push(this);
    }

    ListenFor(item, eventName, where) {
        switch (eventName) {
            case "MousePressed":
                this.ListenForMousePressed(item, where);
                break;
            case "MouseDragged":
                this.ListenForMouseDragged(item, where);
                break;
            case "MouseWheel":
                this.ListenForMouseWheel(item, where);
                break;
            case "MouseReleased":
                this.ListenForMouseReleased(item, where);
                break;
            case "MouseDoubleClick":
                this.ListenForMouseDoubleClick(item, where);
                break;
        }
    }

    ListenForMousePressed(item, where) {
        // item should have an item.x, item.y, item.width and item.height
        // item should have method MousePressed()
        this.itemsRegisteredForMousePressed.push([item, where]);
    }

    ListenForMouseReleased(item, where) {
        // item should have an item.x, item.y, item.width and item.height
        // item should have method MousePressed()
        this.itemsRegisteredForMouseReleased.push([item, where]);
    }

    ListenForMouseDragged(item, where) {
        this.itemsRegisteredForMouseDragged.push([item, where]);
    }

    ListenForMouseWheel(item, where) {
        this.itemsRegisteredForMouseWheel.push([item, where]);
    }

    ListenForMouseDoubleClick(item, where) {
        this.itemsRegisteredForMouseDoubleClicked.push([item, where]);
    }

    _MouseWentDown() {
        console.log("_MouseWentDown");
        this.lastMouseDownVector = new createVector(mouseX, mouseY);
        this.itemsRegisteredForMousePressed.forEach(tup => {
            let item = tup[0];
            let where = tup[1];
            if (
                where == "Anywhere" ||
                (item.x <= mouseX <= item.x + item.width &&
                    item.y <= mouseY <= item.y + item.height)
            ) {
                item.MousePressed();
            }
        });
    }

    _MouseWentDragged() {
        let thisMouseVector = createVector(mouseX, mouseY);
        let deltaX = thisMouseVector.x - this.lastMouseDownVector.x;
        let deltaY = thisMouseVector.y - this.lastMouseDownVector.y;
        console.log("_MouseWentDragged deltaX=", deltaX, " , deltaY=", deltaY);
        this.itemsRegisteredForMouseDragged.forEach(tup => {
            let item = tup[0];
            let where = tup[1];
            if (
                where == "Anywhere" ||
                (item.x <= mouseX <= item.x + item.width &&
                    item.y <= mouseY <= item.y + item.height)
            ) {
                item.MouseDragged(deltaX, deltaY);
            }
        });

        this.lastMouseDownVector = thisMouseVector;
    }

    _MouseWheelTurned(amountTurned) {
        console.log("_MouseWheelTurned(", amountTurned);
        this.itemsRegisteredForMouseWheel.forEach(tup => {
            let item = tup[0];
            let where = tup[1];
            if (
                where == "Anywhere" ||
                (item.x <= mouseX <= item.x + item.width &&
                    item.y <= mouseY <= item.y + item.height)
            ) {
                item.MouseWheel(amountTurned);
            }
        });
    }

    _MouseWentUp() {
        console.log("_MouseWentUp");
        this.lastMouseDownVector = null;
        this.itemsRegisteredForMouseReleased.forEach(tup => {
            let item = tup[0];
            let where = tup[1];
            if (
                where == "Anywhere" ||
                (item.x <= mouseX <= item.x + item.width &&
                    item.y <= mouseY <= item.y + item.height)
            ) {
                item.MouseReleased();
            }
        });
    }

    _MouseWentDoubleClick() {
        console.log("_MouseWentDoubleClick");
        this.itemsRegisteredForMouseDoubleClicked.forEach(tup => {
            let item = tup[0];
            let where = tup[1];

            //   item.MouseDoubleClicked();
            if (
                where == "Anywhere" ||
                (item.x <= mouseX <= item.x + item.width &&
                    item.y <= mouseY <= item.y + item.height)
            ) {
                item.MouseDoubleClicked();
            }
        });
    }

    Update() {}
}

// p5js events /////////////////////////////////////////////////////////
function mousePressed() {
    // console.log("mousePressed", event);
    mouseHandlers.forEach(handler => {
        handler._MouseWentDown();
    });
}

function mouseReleased() {
    // console.log("mouseReleleased");
    mouseHandlers.forEach(handler => {
        handler._MouseWentUp();
    });
}

function mouseDragged() {
    // mouse will only be "dragged" between Press and Release
    // console.log("mouseDragged(", event);
    mouseHandlers.forEach(handler => {
        handler._MouseWentDragged();
    });
}

function mouseWheel(event) {
    // console.log("mouseWheel");
    mouseHandlers.forEach(handler => {
        handler._MouseWheelTurned(event.deltaY);
    });
}

function doubleClicked() {
    // console.log("doubleClicked");
    mouseHandlers.forEach(handler => {
        handler._MouseWentDoubleClick();
    });
}

function keyPressed() {
    // called for special characters like BACKSPACE
    // console.log("keyPressed", key);
    keyboardHandlers.forEach(handler => {
        handler._KeyWentDown();
    });
}

function keyReleased() {
    keyboardHandlers.forEach(handler => {
        handler._KeyWentUp();
    });
}

// scrolling list /////////////////////////////////////////////////////////////
class ScrollingListVertical {
    constructor(x, y, width, height, fill) {
        console.log("constructor.constructor(", x, y, width, height, fill);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this._fill = fill;

        this._listItems = []; // index is z layer (0=back-most layer)
        this.selectedProduct = null;

        this.currentScrollPosition = 0;

        //scroll stuff
        this.desiredScrollPosition = this.y;
        this.currentScrollPosition = this.y;
        this.currentScrollVelocity = 0;
        this.currentScrollAcceleration = 0;
        this.scrollMass = 10;
        this.scrollFriction = 0.5; // 1 = no friction
        this.scrollMaxPosition = this.y;
        this.scrollMinPosition = null;

        // register for mouse events
        if (mouseHandlers.length == 0) {
            new MouseEventHanlder();
        }
        mouseHandlers.forEach(handler => {
            handler.ListenFor(this, "MouseWheel");
            handler.ListenFor(this, "MouseDoubleClick");
            handler.ListenFor(this, "MouseDragged");
        });
    }

    Clear() {
        this._listItems = [];
    }

    Draw() {
        // draw this scrolling list's background
        fill(this._fill);
        // rect(10, 10, 10, 10);
        rect(this.x, this.y, this.width, this.height);

        // let relativeDrawPosition = 5;
        let relativeDrawPosition = this.currentScrollPosition;

        let totalHeight = 0;

        this._listItems.forEach(p => {
            p.Draw(this.x, relativeDrawPosition);
            relativeDrawPosition += p.height;
            totalHeight += p.height;
        });

        this.scrollMinPosition = this.y - totalHeight + this.height;

        //scroll stuff
        let force = this.desiredScrollPosition - this.currentScrollPosition;
        // console.log("force=", force);
        this.currentScrollAcceleration = force / this.scrollMass;
        this.currentScrollVelocity -= this.currentScrollAcceleration;
        this.currentScrollVelocity *= this.scrollFriction;
        this.currentScrollPosition -= this.currentScrollVelocity;

        // scroll indicator
        let scrollDiff = this.currentScrollPosition - this.desiredScrollPosition;
        // console.log("scrollDiff=", scrollDiff);
        if (Math.abs(scrollDiff) > 2) {
            // console.log("current=", this.currentScrollPosition);
            // console.log("desired=", this.desiredScrollPosition);
            let percent =
                100 -
                ToPercent(
                    this.currentScrollPosition,
                    this.scrollMinPosition,
                    this.scrollMaxPosition
                );
            // console.log("percent=", percent);
            let iconSize = 10; // assume same width/height
            let scrollIconPosition = this.y + (percent / 100) * this.height - iconSize / 2;
            // console.log(
            //   "scrollIconPosition=",
            //   scrollIconPosition,
            //   ", this.y+this.height=",
            //   this.y + this.height
            // );
            fill("white");

            rect(
                this.x + this.width - iconSize,
                scrollIconPosition,
                iconSize,
                iconSize,
                10,
                10,
                10,
                10
            );
        }
    }

    AddItem(item) {
        console.log("ProductList.AddItem(", item);

        // add a new ListItem
        this._listItems.push(item);
    }

    IsInside(x, y) {
        // return true if x,y is inside this ScrollingLIst object
        if (x < this.x + this.width && x > this.x && y < this.y + this.height && y > this.y) {
            return true;
        } else {
            return false;
        }
    }

    SetDesiredPosition(postiion) {
        this.desiredScrollPosition = postiion;
        if (this.desiredScrollPosition > this.scrollMaxPosition) {
            this.desiredScrollPosition = this.scrollMaxPosition;
        } else if (this.desiredScrollPosition < this.scrollMinPosition) {
            this.desiredScrollPosition = this.scrollMinPosition;
        }
        // console.log("this.desiredScrollPosition=", this.desiredScrollPosition);
    }

    MouseWheel(deltaY) {
        // console.log("Scroll(", deltaY);
        this.SetDesiredPosition(this.desiredScrollPosition - deltaY);
    }

    MouseDoubleClicked() {
        console.log("ProductList.MouseDoubleClicked");

        this._listItems.forEach(item => {
            if (
                item.x <= mouseX <= item.x + item.width &&
                item.y <= mouseY <= item.y + item.height
            ) {
                item.MouseDoubleClicked();
            }
        });
    }

    MouseDragged(deltaX, deltaY) {
        this.SetDesiredPosition(this.desiredScrollPosition + deltaY);
    }
}
class ScrollingListHorizontal {
    constructor(x, y, width, height, fill) {
        console.log("constructor.constructor(", x, y, width, height, fill);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this._fill = fill;

        this._listItems = []; // index is z layer (0=back-most layer)
        this.selectedProduct = null;

        this.currentScrollPosition = 0;

        //scroll stuff
        this.desiredScrollPosition = this.x;
        this.currentScrollPosition = this.x;
        this.currentScrollVelocity = 0;
        this.currentScrollAcceleration = 0;
        this.scrollMass = 10;
        this.scrollFriction = 0.5; // 1 = no friction
        this.scrollMaxPosition = this.x;
        this.scrollMinPosition = null;

        // register for mouse events
        if (mouseHandlers.length == 0) {
            new MouseEventHanlder();
        }
        mouseHandlers.forEach(handler => {
            handler.ListenFor(this, "MouseWheel");
            handler.ListenFor(this, "MouseDoubleClick");
            handler.ListenFor(this, "MouseDragged");
        });
    }

    Clear() {
        this._listItems = [];
    }

    Draw() {
        // draw this scrolling list's background
        fill(this._fill);
        // rect(10, 10, 10, 10);
        rect(this.x, this.y, this.width, this.height);

        let relativeDrawPosition = this.currentScrollPosition;

        // determine the total width of all items
        let totalWidth = 0;

        this._listItems.forEach(p => {
            p.Draw(relativeDrawPosition, this.y);
            relativeDrawPosition += p.width;
            totalWidth += p.width;
        });

        this.scrollMinPosition = this.y - totalWidth + this.height;

        //scroll stuff
        let force = this.desiredScrollPosition - this.currentScrollPosition;
        // console.log("force=", force);
        this.currentScrollAcceleration = force / this.scrollMass;
        this.currentScrollVelocity -= this.currentScrollAcceleration;
        this.currentScrollVelocity *= this.scrollFriction;
        this.currentScrollPosition -= this.currentScrollVelocity;

        // scroll indicator
        let scrollDiff = this.currentScrollPosition - this.desiredScrollPosition;
        // console.log("scrollDiff=", scrollDiff);
        if (Math.abs(scrollDiff) > 2) {
            // console.log("current=", this.currentScrollPosition);
            // console.log("desired=", this.desiredScrollPosition);
            let percent =
                100 -
                ToPercent(
                    this.currentScrollPosition,
                    this.scrollMinPosition,
                    this.scrollMaxPosition
                );
            // console.log("percent=", percent);
            let iconSize = 10; // assume same width/height
            let scrollIconPosition = this.x + (percent / 100) * this.width - iconSize / 2;
            // console.log(
            //   "scrollIconPosition=",
            //   scrollIconPosition,
            //   ", this.y+this.height=",
            //   this.y + this.height
            // );
            fill("white");

            rect(
                scrollIconPosition,
                this.y + this.height - iconSize,
                iconSize,
                iconSize,
                10,
                10,
                10,
                10
            );
        }
    }

    AddItem(item) {
        console.log("ProductList.AddItem(", item);

        // add a new ListItem
        this._listItems.push(item);
    }

    IsInside(x, y) {
        // return true if x,y is inside this ScrollingLIst object
        if (x < this.x + this.width && x > this.x && y < this.y + this.height && y > this.y) {
            return true;
        } else {
            return false;
        }
    }

    SetDesiredPosition(postiion) {
        this.desiredScrollPosition = postiion;
        if (this.desiredScrollPosition > this.scrollMaxPosition) {
            this.desiredScrollPosition = this.scrollMaxPosition;
        } else if (this.desiredScrollPosition < this.scrollMinPosition) {
            this.desiredScrollPosition = this.scrollMinPosition;
        }
        // console.log("this.desiredScrollPosition=", this.desiredScrollPosition);
    }

    MouseWheel(deltaY) {
        // console.log("Scroll(", deltaY);
        this.SetDesiredPosition(this.desiredScrollPosition - deltaY);
    }

    MouseDoubleClicked() {
        console.log("ProductList.MouseDoubleClicked");

        this._listItems.forEach(item => {
            if (
                item.x <= mouseX <= item.x + item.width &&
                item.y <= mouseY <= item.y + item.height
            ) {
                item.MouseDoubleClicked();
            }
        });
    }

    MouseDragged(deltaX, deltaY) {
        this.SetDesiredPosition(this.desiredScrollPosition + deltaX);
    }
}
class _BaseListItemVertical {
    // this is a very basic list item
    constructor(parent) {
        this.width = parent.width;
        this.padding = parent.height / 100; // space between items
        this._rectHeight = parent.height / 10;
        this._rectWidth = parent.width - this.padding * 2;
        this.height = this._rectHeight + this.padding;
        this.parent = parent;
    }

    Draw(x, y) {
        this.x = x;
        this.y = y;

        let topOfItem = this.y;
        let topOfRect = this.y + this.padding;
        let heightOfRect = this._rectHeight;

        // check if the item has gone off the top of the list
        if (this.y + this.padding < this.parent.y) {
            // this item is to high
            if (this.y + this.padding < this.parent.y - this.height) {
                // the entire item is not visible
                return;
            } else {
                // the item is partially visible
                topOfItem = this.parent.y;
                topOfRect = this.parent.y;
                heightOfRect = this._rectHeight - (this.parent.y - this.y - this.padding);
                // console.log("heightOfRect=", heightOfRect);
                if (heightOfRect < 0) {
                    return;
                }
            }
        }

        // check if the item has gone off the bottom of the list
        if (this.y + this.padding + this._rectHeight > this.parent.y + this.parent.height) {
            // this item is to low
            if (this.y + this.padding >= this.parent.y + this.parent.height) {
                // the entire item is not visible
                return;
            } else {
                // the item is partially visible
                heightOfRect = this.parent.y + this.parent.height - topOfRect;
            }
        }

        fill("white");
        rect(this.x + this.padding, topOfRect, this._rectWidth, heightOfRect);
    }

    MouseDoubleClicked() {
        if (
            this.x <= mouseX &&
            mouseX <= this.x + this.width &&
            this.y <= mouseY &&
            mouseY <= this.y + this.height
        ) {
            console.log("_BaseListItem.MouseDoubleClicked", this);
        }
    }
}
class _BaseListItemHorizontal {
    // this is a very basic list item
    constructor(parent, width) {
        this.padding = 5; //parent.width / 100; // space between items
        this.width = width; // total width of item with padding
        this._rectHeight = parent.height - this.padding * 2;
        this._rectWidth = width - this.padding;
        this.height = this._rectHeight + this.padding;
        this.parent = parent;
    }

    Draw(x, y) {
        this.x = x;
        this.y = y;

        let leftOfItem = this.x;

        let topOfRect = this.y + this.padding;
        let leftOfRect = this.x + this.padding;
        let heightOfRect = this._rectHeight;
        let widthOfRect = this._rectWidth;

        // check if the item has gone off the left of the list
        if (this.x + this.padding < this.parent.x) {
            // this item is too far left
            if (this.x + this.padding < this.parent.x - this.width) {
                // the entire item is not visible
                // dont draw it
                return;
            } else {
                // the item is partially visible
                leftOfItem = this.parent.x;
                leftOfRect = this.parent.x;
                widthOfRect = this._rectWidth - (this.parent.x - this.x - this.padding);
                // console.log("heightOfRect=", heightOfRect);
                if (widthOfRect < 0) {
                    // dont draw it
                    return;
                }
            }
        }

        // check if the item has gone off the right side of the list
        if (this.x + this.padding + this._rectWidth > this.parent.y + this.parent.width) {
            // this item is to too far right
            if (this.x + this.padding >= this.parent.x + this.parent.width) {
                // the entire item is not visible
                // dont draw it
                return;
            } else {
                // the item is partially visible
                widthOfRect = this.parent.x + this.parent.width - leftOfRect;
            }
        }

        fill("white");
        rect(leftOfRect, topOfRect, widthOfRect, heightOfRect);
    }

    MouseDoubleClicked() {
        if (
            this.x <= mouseX &&
            mouseX <= this.x + this.width &&
            this.y <= mouseY &&
            mouseY <= this.y + this.height
        ) {
            console.log("_BaseListItem.MouseDoubleClicked", this);
        }
    }
}
//helper functions ////////////////////////////////////////////////////////////
function time() {
    return millis() / 1000;
}

function IsPrintable(key_code) {
    let isPrintable =
        (key_code > 47 && key_code < 58) || // number keys
        key_code == 32 || // spacebar & return key(s) (if you want to allow carriage returns)
        (key_code > 64 && key_code < 91) || // letter keys
        (key_code > 95 && key_code < 112) || // numpad keys
        (key_code > 185 && key_code < 193) || // ;=,-./` (in order)
        (key_code > 218 && key_code < 223); // [\]' (in order)
    return isPrintable;
}

function ToPercent(value, min, max) {
    // console.log("ToPercent(", value, min, max);
    if (value < min) {
        return 0;
    } else if (value > max) {
        return 100;
    }

    let totalRange = max - min;
    let fromMinToValue = value - min;
    return (fromMinToValue / totalRange) * 100;
}

class Button {
    constructor() {
        this._visible = true;
        this._text = "test";

        this.width = 0;
        this.height = 0;
        this.x = 0;
        this.y = 0;
    }
    SetText(text) {
        this._text = text;
    }
    Draw(x, y, width, height) {
        if (this._visible) {
            this.x = x;
            this.y = y;
            (this.width = width), (this.height = height);

            stroke(1);
            fill("white");
            rect(
                x,
                y,
                width,
                height,
                5 // rounded endges
            );
            fill("black");
            noStroke();
            textAlign(CENTER, CENTER);
            text(this._text, this.x + this.width / 2, this.y + this.height / 2);
        }
    }
}

function GetMaxHeightByAspectRatio(width, height, ratio) {
    let maxHeight = width / ratio;
    if (maxHeight > height) {
        return height;
    } else {
        return maxHeight;
    }
}

function GetMaxWidthByAspectRatio(width, height, ratio) {
    maxWidth = ratio * height;
    if (maxWidth > width) {
        return width;
    } else {
        return maxWidth;
    }
}
